import React, {Component} from 'react';
import api from '../services/api';
import './Login.css'
import { history } from '../History'

class Login extends Component{
    state = {
        username: [],
    }
    
    handleClick() {
        history.push('/pokemon')
        window.location.reload();
    }
    
    loadLogin = async({username}) => {
        const response = await api.get(`/users/${username}`);
        console.log(response);
    }
    newUser = async (event) => {
        event.preventDefault();
        const {username} = this.state;
        const newRegister = await api.post("/users", {username});
        this.setState({username: newRegister.username});
        console.log(newRegister)
        localStorage.setItem('username', JSON.stringify({username}))
        this.handleClick();
    }
    render(){
        return(
            <div className="container-login">
                <div className="box">
                    <form className="form" onSubmit={this.newUser}>
                        <input type='text' placeholder='digite o usuário' value={this.state.username}
                        onChange={
                            event=>{
                                this.setState({username: event.target.value})
                            }
                        }></input>
                        <button type='submit'> Enviar </button>
                    </form>
                </div>
                </div>
        );
    }
}
export default Login;