import React, {Component} from 'react';
import api from '../services/api';
import "./Pokemon.css";
import Navbar from "../Components/Navbar"
import {Link} from 'react-router-dom'


class Main extends Component{
    state = {
        pokemons: [],
        pokemonInfo: {},
        page: 1,
    }
    componentDidMount(){
        this.loadPokemon();
    }
    loadPokemon = async(page = 1) =>{
        const response = await api.get(`/pokemons?page=${page}`);
        const {data, ...pokemonInfo} = response.data;
        this.setState({pokemons: data, pokemonInfo});
        console.log(response.data)
    }

    anterior = () =>{
        const {page, pokemonInfo} = this.state;
        if(pokemonInfo.prev_page === null) return;
        const pageNumber = page - 1;
        this.setState({page: pageNumber});
        this.loadPokemon(pageNumber);
    }
    proxima = () =>{
        const {page, pokemonInfo} = this.state;
        if(pokemonInfo.next_page === null) return;
        const pageNumber = page + 1;
        this.setState({page: pageNumber});
        this.loadPokemon(pageNumber)
    }
   favoritar = (pokemon) =>{
       const user = JSON.parse(localStorage.getItem('username'));
        api.post(`users/${user.username}/starred/${pokemon}`)
   }
    render(){
        const username = JSON.parse(localStorage.getItem('username'));
        return(
            <div className="Page-Pokemon">
                <Navbar></Navbar>
                <div className="all">
                    <p> Bem vindo(a), {username.username}! </p>
                    <div className="pokedex">
                        {this.state.pokemons.map(pokemon =>(
                                <div key={pokemon.id} className="pokemon">
                                    <img src= {pokemon.image_url} alt="pokemon" className="image" />
                                    <Link className="Link" to = {`/pokemon/${pokemon.name}`}>
                                        <span> {pokemon.name} </span>
                                    </Link>
                                    <i onClick={()=>(this.favoritar(pokemon.name))} class="fa fa-heart-o" aria-hidden="true"></i>
                                </div>
                        ))}
                    </div>
                    <button onClick={this.anterior} className="btn-pokemon"> Anterior </button>
                    <button onClick={this.proxima} className="btn-pokemonp"> Próxima </button>
                </div> 
            </div>
        )
    }
}
export default Main;