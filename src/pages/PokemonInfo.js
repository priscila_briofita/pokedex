import React, {useState, useEffect} from 'react'
import api from '../services/api'
import Navbar from '../Components/Navbar'
import './PokemonInfo.css'
import Footer from '../Components/Footer'

function Favorito (props){
    const [pokemons, setPokemons] = useState({});
    let pokemon = props.match.params.pokemon
    useEffect(() =>{
        api.get(`/pokemons/${pokemon}`)
        .then(
            (res) =>{
                setPokemons(res.data)
            }
        )
    },[pokemon])
    return(
        <div className="tudo"> 
            <Navbar></Navbar>
            <div className="container">
                <p> Informações </p>
                <div className="card">
                    <img src={pokemons.image_url} alt="pokemon"/>
                    <div className="pokeInfo">
                        <span>{pokemons.name}</span>
                        <span className="kind">{pokemons.kind}</span>
                    </div>
                </div>
            </div>
            <Footer></Footer>
        </div>
    )
}
export default Favorito;