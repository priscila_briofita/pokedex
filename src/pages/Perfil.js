import React, {Component} from 'react';
import api from '../services/api';
import Navbar from '../Components/Navbar';
import Footer from '../Components/Footer';
import {Link} from 'react-router-dom'
import './Perfil.css'

class Perfil extends Component{
    state = {
        user: [],
        pokemons: [], 
        pao: false
    }
    componentDidMount(){
        this.exibe();
    }
    exibe = ()=>{
        const user = JSON.parse(localStorage.getItem("username"));
        api.get(`/users/${user.username}`)
        .then( (response) => {
            this.setState({pokemons: response.data.pokemons})
            console.log(response.data.pokemons)
        }, [])
        console.log(user);
    }
    delete = (pokemon) =>{
        const user = JSON.parse(localStorage.getItem('username'));
        api.delete(`/users/${user.username}/starred/${pokemon}`)
        this.exibe();
    }

    render(){
        const user = JSON.parse(localStorage.getItem('username'));
        return(
            <div className="Perfil">
                <Navbar></Navbar>
                <div className="background">
                    <i class="fas fa-user-circle" aria-hidden="true"></i>
                    <p> Usuário: {user.username} </p>
                    <div className="pokedex2">
                            {this.state.pokemons.map(pokemon =>(
                                <div key={pokemon.id} className="pokemons"> 
                                    <img src={pokemon.image_url} alt="pokemon"/>
                                    <Link className="Link" to = {`/pokemon/${pokemon.name}`}>
                                        <span> {pokemon.name} </span>
                                    </Link>
                                    <i onClick={()=> (this.delete(pokemon.name))} class="fas fa-trash" aria-hidden="true"></i>
                                </div>
                            ))}
                    </div>
                </div>
                <Footer></Footer>
            </div>
        )
    }
}
export default Perfil;