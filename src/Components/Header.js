import React, {Component} from 'react';
import './Header.css'

class Header extends Component{
    render(){
        return(
            <div className="Head">
                <img src="https://i.pinimg.com/originals/18/d9/e1/18d9e1307018dbc76750ca7d5124fccd.png"
                     alt="poke" className="poke"/>
                <p> Pokedex </p>
            </div>
        );
    }
}
export default Header;