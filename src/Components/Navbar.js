import React from 'react'
import './Navbar.css'
import {Link} from 'react-router-dom'

const Navbar = () => {
    const user = JSON.parse(localStorage.getItem("username"))
    return (
    <header className='divnavbar'>
        <div className="logo">
            <img src="https://i.pinimg.com/originals/18/d9/e1/18d9e1307018dbc76750ca7d5124fccd.png"
                        alt="poke" className="poke"/>
            <p> Pokedex </p>
        </div>
        <ul className="ul-navbar">
            <Link to={`/Pokemon`} className="li-navbar"> Pokedex </Link>
            <Link to={`/users/${user.username}`} className="li-navbar"> Perfil </Link>
        </ul>
    </header>
 );
}

export default Navbar;