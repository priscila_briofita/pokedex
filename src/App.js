import React from 'react';
import './App.css';
import Login from './pages/Login'
import Header from './Components/Header'
import Footer from "./Components/Footer"

function App(){
  return(
    <div className="app">
      <Header></Header>
      <Login></Login>
      <Footer></Footer>
    </div>
  )
}

export default App;
