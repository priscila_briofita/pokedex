import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Switch, Route} from 'react-router-dom';
import Pokemon from './pages/Pokemon'
import Perfil from './pages/Perfil'
import Notfound from './pages/NotFound'
import Favorito from './pages/PokemonInfo'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Switch>
        <Route path="/" exact={true} component={App}/>
        <Route path="/pokemon" exact={true} component={ Pokemon }/>
        <Route path="/users" component={ Perfil }/>
        <Route path="/pokemon/:pokemon" component={ Favorito }/>
        <Route path="*" component={Notfound}/>
      </Switch>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
